#Conway's Game of Life

###Description

The "game" is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves or, for advanced players, by creating patterns with particular properties.

###SETUP

    git clone https://github.com/vjdhama/gameoflife3.git
    cd game_of_life
    bundle install

###BUILD

    bundle exec rake

###RUN SPECIFIC SPECS  
  
    bundle exec rspec [SPECFILE_PATH:LINE]

###LAUNCH
