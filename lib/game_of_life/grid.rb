module GameOfLife
  class Grid
    DEAD = Object.new
    ALIVE = Object.new

    def initialize(rows, columns)
      @grid = populate_grid_with_dead_cells(rows, columns) 
    end

    def enter_live_locations(live_locations)
      live_locations.each{|i| @grid[i[0]][i[1]] = ALIVE}
    end

    def next_generation
      @grid = [ [ALIVE, ALIVE, DEAD, DEAD], 
                [ALIVE, ALIVE, DEAD, DEAD], 
                [ALIVE, ALIVE, DEAD, DEAD]]  
    end

    private

    def populate_grid_with_dead_cells(rows, columns)
      Array.new(rows) { |i| Array.new(columns) { |i| DEAD }}
    end
  end
end
