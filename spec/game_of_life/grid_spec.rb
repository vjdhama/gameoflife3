require 'spec_helper'

module GameOfLife
  describe Grid do
    it "should generate next generation" do
      grid = Grid.new(3, 4)
      live_locations = [[0, 0], [1, 0], [2,0], [1, 1]]
      grid.enter_live_locations(live_locations)
      old_grid = grid
      new_grid = grid.next_generation
      expect(new_grid).to_not eq(old_grid)
    end

    it "next generation based on [[0, 0], [1, 1], [1,2], [2, 2]] should be 
                [[ALIVE, ALIVE, DEAD, DEAD], 
                [ALIVE, ALIVE, DEAD, DEAD], 
                [ALIVE, ALIVE, DEAD, DEAD]] " do
      ALIVE = Grid::ALIVE
      DEAD = Grid::DEAD
      grid = Grid.new(3, 4)
      live_locations = [[0, 0], [1, 1], [1,2], [2, 2]]
      grid.enter_live_locations(live_locations)
      grid = grid.next_generation
      new_grid = [[ALIVE, ALIVE, DEAD, DEAD], [ALIVE, ALIVE, DEAD, DEAD], [ALIVE, ALIVE, DEAD, DEAD]]
      expect(grid).to eq(new_grid)
    end
  end
end
